package com.example.demo.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class UserEntity extends AbstractAuditingEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@NotEmpty
	@Column(nullable = false, name = "username", unique = true)
	private String username;
	
	@NotEmpty
	@Column(name = "fullname")
	private String fullName;
	
	@NotEmpty
	@Column(nullable = false, name = "password", unique = true)
	private String password;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "role_name", referencedColumnName = "parent") })
	@JsonIgnore
    private Set<RoleEntity> user_role;

	
	public UserEntity() {
		super();
	}

	public UserEntity(@NotEmpty String username, @NotEmpty String fullName) {
		super();
		this.username = username;
		this.fullName = fullName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Set<RoleEntity> getUser_role() {
		return user_role;
	}

	public void setUser_role(Set<RoleEntity> user_role) {
		this.user_role = user_role;
	}

		
}
