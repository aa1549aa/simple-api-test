package com.example.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "r")
public class RoleEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "parent")
	private String name;
	
	@Column(name = "child")
	@NotEmpty
	private String child;;

	public RoleEntity() {
		super();
	}

	public RoleEntity(String name, @NotEmpty String child) {
		super();
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getChild() {
		return child;
	}

	public void setChild(String child) {
		this.child = child;
	}

}
