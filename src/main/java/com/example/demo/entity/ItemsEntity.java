package com.example.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "items")
public class ItemsEntity implements Serializable, GrantedAuthority{

private static final long serialVersionUID = 1L;
	
	private final Short PERMISSION = 2;
	private final Short ROLE = 1;
	
	@Id
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	@NotEmpty
	private Short type;

	public ItemsEntity() {
		super();
	}

	public ItemsEntity(String name, @NotEmpty Short type) {
		super();
		this.name = name;
		this.type = type;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getRole() {
		return name;
	}

	
	public void setRole(String name) {
		this.name = name;
		this.type = this.ROLE;
	}
	
	

}
