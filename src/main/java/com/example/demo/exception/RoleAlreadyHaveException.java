package com.example.demo.exception;

public class RoleAlreadyHaveException extends ApiException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RoleAlreadyHaveException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RoleAlreadyHaveException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}
