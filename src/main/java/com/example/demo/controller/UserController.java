package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.UserNotFoundException;
import com.example.demo.payload.request.UserRequestCreate;
import com.example.demo.payload.request.UserRequestDelete;
import com.example.demo.payload.request.UserRequestUpdate;
import com.example.demo.payload.response.UserResponse;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "/create")
	public ResponseEntity<UserResponse> userCreateController(@RequestBody UserRequestCreate userRequestCreate) {
		
		return ResponseEntity.ok(userService.create(userRequestCreate));		
	}
	
	@PostMapping(value = "/update")
	public ResponseEntity<UserResponse> userUpdateController(@RequestBody UserRequestUpdate userRequestUpdate) throws UserNotFoundException {
		
		return ResponseEntity.ok(userService.update(userRequestUpdate));		
	}
	
	@PostMapping(value = "/delete")
	public ResponseEntity<String> userDeleteController(@RequestBody UserRequestDelete userRequestDelete) throws UserNotFoundException {
		
		return ResponseEntity.ok(userService.delete(userRequestDelete));		
	}
}
