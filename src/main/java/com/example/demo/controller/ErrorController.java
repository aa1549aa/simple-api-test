package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;

import com.example.demo.exception.UserNotFoundException;
import com.example.demo.payload.response.ErrorResponse;


@RestControllerAdvice
public class ErrorController{
	
	@ExceptionHandler(UserNotFoundException.class)
//	@ApiResponses(value = {
//            @ApiResponse(code = 404, message = "User Not Found", response = UserNotFoundException.class)
//    })
	public ResponseEntity<ErrorResponse> userNotFoundEx(Exception e, ServletWebRequest webRequest){
		return new ResponseEntity<>(
				new ErrorResponse(
                        HttpStatus.NOT_FOUND.value(),
                        e.getClass().getSimpleName(),
                        webRequest.getRequest().getRequestURI(),
                        e.getLocalizedMessage()),
                HttpStatus.NOT_FOUND);
	}
}
