package com.example.demo.controller;
		
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class TestController {

	@PostMapping("/hello")
	public String hello(HttpServletRequest httpServletRequest) {
		return "Hello there!";
	}
}
