package com.example.demo.convertor;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.example.demo.entity.RoleEntity;
import com.example.demo.payload.request.RoleRequestCreate;
import com.example.demo.payload.response.RoleResponse;

@Component
public class RoleConvertor extends Convertor<RoleRequestCreate, RoleResponse>{

	public RoleConvertor(Function<RoleResponse, RoleEntity> fromDto, Function<RoleEntity, RoleResponse> fromEntity) {
		
		super(request -> new RoleResponse(request.getRoleName(), request.getPermissions()),
				response -> new RoleRequestCreate(response.getRoleName(), response.getPermissions()));
		
	}
	
	

}
