package com.example.demo.convertor;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class Convertor<D, E> {
	private final  Function<E, D> fromEntity;
	private final Function<D, E> fromDto;
	
	public Convertor(final  Function<D, E> fromDto, final Function<E, D> fromEntity) {
		super();
		this.fromDto = fromDto;
		this.fromEntity = fromEntity;
	}
	
	public final E convertFromDto(final D d) {
		return fromDto.apply(d);
	}
	
	public final D convertFromEntity(final E e) {
		return fromEntity.apply(e);
	}
	
	public final List<E> convertfromDtos(final Collection<D> dtos){
		return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
	}
	
	public final List<D> convertFromEntities(Collection<E> entities){
		return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
	}
	
	public final Page<D> convertFromPageEntity(Page<E> page){
		return new PageImpl<>(page.getContent().stream().map(this :: convertFromEntity).collect(Collectors.toList()), 
				page.getPageable(), page.getTotalElements());
	}
	
}
