package com.example.demo.convertor;


import org.springframework.stereotype.Component;

import com.example.demo.entity.UserEntity;
import com.example.demo.payload.request.UserRequestCreate;
import com.example.demo.payload.response.UserResponse;

@Component
public class UserConvertor extends Convertor<UserRequestCreate, UserResponse>{

	public UserConvertor() {
		super(request -> new UserResponse(request.getUsername(), request.getFullName()), 
				response -> new UserRequestCreate(response.getUsername(), response.getFullName()));
		
	}
	
	public UserResponse convertEntitytoResponse(UserEntity userEntity) {
		return new UserResponse(userEntity.getId(), 
				userEntity.getUsername(), 
				userEntity.getFullName(),
				userEntity.getUser_role().getName());
	}
	

}
