package com.example.demo.payload.response;

public class UserResponse {
	
	private Long id;
	private String username;
	private String fullName;
	private String role;
	
	
	public UserResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserResponse(Long id, String username, String fullNaem) {
		super();
		this.id = id;
		this.username = username;
		this.fullName = fullNaem;
	}
	

	public UserResponse(String username, String fullName) {
		super();
		this.username = username;
		this.fullName = fullName;
	}

	
	public UserResponse(Long id, String username, String fullName, String role) {
		super();
		this.id = id;
		this.username = username;
		this.fullName = fullName;
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
	
	
}
