package com.example.demo.payload.response;

public class RoleResponse {
	
	private String roleName;
	private String[] permissions;
	
	public RoleResponse() {
		super();
	}

	public RoleResponse(String roleName, String[] permissions) {
		super();
		this.roleName = roleName;
		this.permissions = permissions;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String[] getPermissions() {
		return permissions;
	}

	public void setPermissions(String[] permissions) {
		this.permissions = permissions;
	}
	
	
}
