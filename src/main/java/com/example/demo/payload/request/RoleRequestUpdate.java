package com.example.demo.payload.request;

public class RoleRequestUpdate {
	
	private String oldName;
	private String newName;
	
	
	public RoleRequestUpdate() {
		super();
	}


	public RoleRequestUpdate(String oldName, String newName) {
		super();
		this.oldName = oldName;
		this.newName = newName;
	}


	public String getOldName() {
		return oldName;
	}


	public void setOldName(String oldName) {
		this.oldName = oldName;
	}


	public String getNewName() {
		return newName;
	}


	public void setNewName(String newName) {
		this.newName = newName;
	}
	
	
}
