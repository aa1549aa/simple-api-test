package com.example.demo.payload.request;

public class RoleRequestCreate {
	
	private String roleName;
	private String[] permissions;
	
	public RoleRequestCreate() {
		super();
	}

	
	public RoleRequestCreate(String roleName, String[] permissions) {
		super();
		this.roleName = roleName;
		this.permissions = permissions;
	}


	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String[] getPermissions() {
		return permissions;
	}

	public void setPermissions(String[] permissions) {
		this.permissions = permissions;
	}
	
	
	
}
