package com.example.demo.payload.request;

public class UserRequestCreate {

	private String username;
	private String fullName;
	private String password;
	private Long roleId;
	
	public UserRequestCreate() {
		super();
	}
	
	
	public UserRequestCreate(String username, String fullName, String password, Long roleId) {
		super();
		this.username = username;
		this.fullName = fullName;
		this.password = password;
		this.roleId = roleId;
	}

	
	public UserRequestCreate(String username, String fullName) {
		super();
		this.username = username;
		this.fullName = fullName;
	}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "UserRequestCreate [username=" + username + ", fullName=" + fullName + ", password=" + password
				+ ", roleId=" + roleId + "]";
	}
	
	
	
}
