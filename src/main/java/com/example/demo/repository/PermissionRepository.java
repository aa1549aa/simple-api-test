package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.ItemsEntity;

@Repository
public interface PermissionRepository extends JpaRepository<ItemsEntity, Long>{
	
	ItemsEntity findByPermission(String permission);
}
