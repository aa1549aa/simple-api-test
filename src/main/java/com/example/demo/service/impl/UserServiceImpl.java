package com.example.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.convertor.UserConvertor;
import com.example.demo.entity.RoleEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.payload.request.UserRequestCreate;
import com.example.demo.payload.request.UserRequestDelete;
import com.example.demo.payload.request.UserRequestUpdate;
import com.example.demo.payload.response.UserResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder; 
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserConvertor userConvertor;
	
	@Override
	public UserResponse create(UserRequestCreate userRequestCreate) {
		Long roleId = userRequestCreate.getRoleId();
		
		UserEntity userEntity = new UserEntity(userRequestCreate.getUsername(), userRequestCreate.getFullName());
		
		userEntity.setPassword(passwordEncoder.encode(userRequestCreate.getPassword()));
		
		Optional<RoleEntity> userRole = roleRepository.findById(roleId);
		
		userEntity.setUser_role(userRole.get());
		
		return userConvertor.convertEntitytoResponse(userRepository.save(userEntity));
	}

	@Override
	public UserResponse update(UserRequestUpdate userRequestUpdate) throws UserNotFoundException{
		
		UserEntity userEntity = userRepository.findById(userRequestUpdate.getId())
				.orElseThrow(()->new UserNotFoundException("User not found"));
		
		userEntity.setUsername(userRequestUpdate.getUsername());
		userEntity.setFullName(userRequestUpdate.getFullName());
		
		return userConvertor.convertEntitytoResponse(userRepository.save(userEntity));
	}

	@Override
	public String delete(UserRequestDelete userRequestDelete) throws UserNotFoundException{
		
		if(!userRepository.existsById(userRequestDelete.getId())) {
			throw new UserNotFoundException("User Not Found");
		}
		
		userRepository.deleteById(userRequestDelete.getId());
		return "User Deleted";
	}

}
