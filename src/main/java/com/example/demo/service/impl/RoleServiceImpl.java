package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.convertor.RoleConvertor;
import com.example.demo.entity.RoleEntity;
import com.example.demo.exception.RoleNotFoundException;
import com.example.demo.payload.request.RoleRequestCreate;
import com.example.demo.payload.request.RoleRequestUpdate;
import com.example.demo.payload.response.RoleResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.service.RoleService;

@Service
public class RoleServiceImpl  implements RoleService{
	

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private RoleConvertor roleConvertor;
	
	@Override
	public RoleResponse create(RoleRequestCreate roleRequestCreate) {
		
	}

	@Override
	public RoleResponse update(RoleRequestUpdate roleRequestUpdate) throws RoleNotFoundException {
		
		RoleEntity roleEntity = roleRepository.findByName(roleRequestUpdate.getOldName())
				.orElseThrow(() -> new RoleNotFoundException("Role Not Found"));
		roleEntity.setName(roleRequestUpdate.getNewName());
		return roleConvertor.convertEntitytoResponse(roleRepository.save(roleEntity));
		
	}

}
