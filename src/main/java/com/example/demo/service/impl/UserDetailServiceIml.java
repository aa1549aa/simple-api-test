package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.entity.UserEntity;

@Service
public class UserDetailServiceIml implements UserDetailsService{

	@Autowired
	private com.example.demo.repository.UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByUsername(username); 
		if(userEntity != null) {
			return new UserDetailImpl(userEntity);
		}
		throw new UsernameNotFoundException("username not Found");
	}

}
