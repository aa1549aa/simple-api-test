package com.example.demo.service;

import com.example.demo.exception.RoleNotFoundException;
import com.example.demo.payload.request.RoleRequestCreate;
import com.example.demo.payload.request.RoleRequestUpdate;
import com.example.demo.payload.response.RoleResponse;

public interface RoleService {

	RoleResponse create(RoleRequestCreate roleRequestCreate);
	
	RoleResponse update(RoleRequestUpdate roleRequestUpdate) throws RoleNotFoundException;
}
