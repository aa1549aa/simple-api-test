package com.example.demo.service;

import com.example.demo.exception.UserNotFoundException;
import com.example.demo.payload.request.UserRequestCreate;
import com.example.demo.payload.request.UserRequestDelete;
import com.example.demo.payload.request.UserRequestUpdate;
import com.example.demo.payload.response.UserResponse;

public interface UserService {
	
	UserResponse create(UserRequestCreate userRequestCreate);
	
	UserResponse update(UserRequestUpdate userRequestUpdate) throws UserNotFoundException;
	
	String delete(UserRequestDelete userRequestDelete) throws UserNotFoundException;
}
