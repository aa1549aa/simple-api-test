package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.payload.request.UserRequestCreate;
import com.example.demo.service.UserService;
import com.example.demo.service.impl.UserServiceImpl;

@SpringBootApplication
public class DemoFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoFirstApplication.class, args);
		
		
	}

}
